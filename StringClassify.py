"""
    This script will classify strings a "GOOD" or "BAD"

    Based the following

    1. if vowel => 3: String is BAD
    2. if consonant => 5: string is BAD
    3. Otherwise: String is GOOD

    With two function, one to make sure the input entered are purely strings
    and the other to make check for vowel and return "0" and "1" for consonant

    @Aurthor: Frank U.N.
    @Date: 1th July, 2019
"""

def main():
    """
        This is the main Function
    """

    #Checking for alphabet
    print("checking for Alphabet...\n")
    entryTime = 0
    while (entryTime < 3):
        # Prompt for input from user
        string = input("Enter only string without space: ")
        entryTime += 1

        if alphaCheck(string):
            break

        elif entryTime == 3:
            print("Error Entry,", entryTime, "times")
            break
        else:
            continue
            print("")
    
    print("")


    stringList = []

    #converts the string into a list
    for char in string:
        stringList.append(char)


    cCounter = 0    # counter for consonant
    vCounter = 0    #counter for vowel

    
    # loops through the list and checks for vowels and consonant and increase it.
    for vChar in stringList:
        if vowelCheck(vChar) == 0:
            vCounter +=1
            print("returning 0:", vChar, "is vowel and the", vCounter, "consecutive vowel occurance")


            if vCounter == 3:
                print ("\nwe have more than", vCounter, "consecutive occuring vowels")
                print("[-]", string,  "is a BAD string!")
                break

            # reinitiallize the value of the consonant counter to zero each time a vowel is encountered
            cCounter = 0

        elif vowelCheck(vChar) == 1:
            cCounter +=1
            print("returning 1:", vChar, "is consonant and the", cCounter, "consecutive consonant occurance")

            if cCounter == 5:
                print ("\nwe have more than", cCounter, "consecutive occuring consonants")
                print("[-]", string,  "is a BAD string!")
                break

            elif (vCounter < 3 and cCounter < 5):
                print("\n[+]", string, ":is a GOOD string\a")

            # reinitiallize the value of the vowel counter to zero each time a consonant is encountered
            vCounter = 0
        

def alphaCheck(string):
    """
        This check for alphabets contained in a string input, making sure there are from
        
    """
    if string.isalpha():
        print('"[+]',string,'"->', "True! string is correctly enterd.")
        return True         # Return True if it is between a to z
    else:
        print('"[+]',string,'"->',"False, String Wrongly entered.\n")

    return False            # Return False if it is otherwise


def vowelCheck( vChar):
    """
        This function check for vowel letters
        And returns 0 if true
        And returns 1 if false

        vowel sounds are [a, e, i, o, u]
    """

    vowel = ['a', 'e', 'i', 'o', 'u']
    
    # counters to hold both vowel and consonants
    if vChar in vowel:
        return 0
    return 1
    



if __name__ == "__main__":
    main()






